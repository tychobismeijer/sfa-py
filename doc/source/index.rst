.. Functional Sparse-Factor Analysis documentation master file, created by
   sphinx-quickstart on Tue Nov 27 15:44:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Functional Sparse-Factor Analysis's documentation
=================================================



This package implements the core algorithm in functional sparse-factor analysis
(funcSFA) [bismeijer2018]_. It calculates a funcSFA solution for a fixed set of parameters.
These paramters need to be optimized for a dataset, for example by a grid search.
For the functional analysis of the result we have implemented gene-set
enrichment analysis in the R package flexgsea [bismeijer2017]_.

.. toctree::
   :maxdepth: 2

   api

References
----------

.. [bismeijer2018] Bismeijer, Tycho, Sander Canisius, and Lodewyk F. A. Wessels. 2018. "Molecular Characterization of Breast and Lung Tumors by Integration of Multiple Data Types with Functional Sparse-Factor Analysis." PLOS Computational Biology 14 (10): e1006520. `doi:10.1371/journal.pcbi.1006520 <https://doi.org/10.1371/journal.pcbi.1006520>`_.

.. [bismeijer2017] Bismeijer, Tycho and Yongsoo Kim. 2017. "FlexGSEA". Zenodo. `doi:10.5281/zenodo.1182639 <https://doi.org/10.5281/zenodo.1182639>`_.
