FuncSFA API
===========

.. module:: funcsfa

SFA
---

.. autoclass:: SFA()
    :members:

DataMatrix
----------

.. autoclass:: DataMatrix
    :members:

StackedDataMatrix
-----------------

.. autoclass:: StackedDataMatrix
    :members:
